NAME = $@
REFRESH_TIME ?= 2m
PLUGIN_PREFIX ?= $(shell whoami)_
PLUGIN_DIRECTORY = "$(HOME)/Library/Application Support/xbar/plugins"
PLUGIN_NAME = $(PLUGIN_PREFIX)$(NAME).$(REFRESH_TIME).sh
SOURCE_DIRECTORY = $(shell pwd)

dev: setup
	# this target exists for convenience during development, replace the plugin script with the one you're working on
	source env.sh && SOURCE_DIRECTORY=$(SOURCE_DIRECTORY) venv/bin/python plugin.py

install: setup plugin

plugin:
	$(call generate_script)

setup:
	python3 -m venv venv
	venv/bin/pip --quiet install --upgrade pip
	venv/bin/pip --quiet install --requirement requirements.txt
	@test -d $(PLUGIN_DIRECTORY) || (echo "\nPlease install xbar and start it at least once to create the plugin directory!"; exit 1)

uninstall:
	find $(PLUGIN_DIRECTORY) -type l -lname "$(SOURCE_DIRECTORY)*" -delete
	rm -rf $(SOURCE_DIRECTORY)/scripts
	rm -rf venv

define generate_script
	@mkdir -p $(SOURCE_DIRECTORY)/scripts
    @echo "#!/bin/bash" > $(SOURCE_DIRECTORY)/scripts/$(PLUGIN_NAME)
	@echo "cd $(SOURCE_DIRECTORY) || exit 1" >> $(SOURCE_DIRECTORY)/scripts/$(PLUGIN_NAME)
	@echo "export SOURCE_DIRECTORY=$(SOURCE_DIRECTORY)" >> $(SOURCE_DIRECTORY)/scripts/$(PLUGIN_NAME)
	@echo "source env.sh" >> $(SOURCE_DIRECTORY)/scripts/$(PLUGIN_NAME)
	@echo "venv/bin/python $(NAME).py" >> $(SOURCE_DIRECTORY)/scripts/$(PLUGIN_NAME)
	@chmod +x $(SOURCE_DIRECTORY)/scripts/$(PLUGIN_NAME)
	@ln -fs $(SOURCE_DIRECTORY)/scripts/$(PLUGIN_NAME) $(PLUGIN_DIRECTORY)/$(PLUGIN_NAME)
endef
