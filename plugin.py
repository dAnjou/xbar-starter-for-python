import datetime
import os

import humanize
from bs4 import BeautifulSoup
import requests

from xbar import Output


def status():
    base_url = 'https://gitlab.com/dAnjou/xbar-starter-for-python'
    response = requests.get(base_url + '/-/commits/master')
    soup = BeautifulSoup(response.text, features="html.parser")
    last_commit = datetime.datetime.strptime(soup.find('time').get('datetime'), '%Y-%m-%dT%H:%M:%SZ')
    print('🤩 dAnjou/xbar-starter-for-python')
    print('---')
    src_dir = os.getenv('SOURCE_DIRECTORY')
    if src_dir:
        print(f'My source directory is {src_dir}')
    print(Output(f'Updated {humanize.naturaldelta(last_commit)} ago').color('green').href(base_url))


if __name__ == '__main__':
    status()
