from __future__ import annotations

from dataclasses import dataclass


@dataclass
class OutputOption:
    key: str
    value: str

    def __str__(self):
        return f'{self.key}={self.value}'

    def __hash__(self):
        return hash(self.key)


class Output:
    def __init__(self, output: str):
        self.output = output
        self._options = set()

    def key(self, value: str) -> Output:
        """Add a key shortcut
        - Use `+` to create combinations
        - Example options: `CmdOrCtrl`, `OptionOrAlt`, `shift`, `ctrl`, `super`, `tab`, `plus`, `return`, `escape`, `f12`, `up`, `down`, `space`"""
        return self.option('key', value)

    def href(self, value: str) -> Output:
        """Make the item clickable"""
        return self.option('href', value)

    def color(self, value: str) -> Output:
        """Change their text color. eg. `red` or `#ff0000`"""
        return self.option('color', value)

    def font(self, value: str) -> Output:
        """Change their text font. eg. `UbuntuMono-Bold`"""
        return self.option('font', value)

    def size(self, value: int) -> Output:
        """Change their text size"""
        return self.option('size', str(value))

    def refresh(self) -> Output:
        """Make the item refresh the plugin it belongs to. If the item runs a script, refresh is performed after
        the script finishes"""
        return self.option('refresh', self._bool(True))

    def no_dropdown(self) -> Output:
        """The line will only appear and cycle in the status bar but not in the dropdown"""
        return self.option('dropdown', self._bool(False))

    def length(self, value: int) -> Output:
        """Truncate the line to the specified number of characters. A `…` will be added to any truncated strings,
        as well as a tooltip displaying the full string"""
        return self.option('length', str(value))

    def no_trim(self) -> Output:
        """No trimming of leading/trailing whitespace from the title"""
        return self.option('trim', self._bool(False))

    def alternate(self) -> Output:
        """Mark a line as an alternate to the previous one for when the Option key is pressed in the dropdown"""
        return self.option('alternate', self._bool(True))

    def templateImage(self, value: str) -> Output:
        """Set an image for this item. The image data must be passed as base64 encoded string and should consist of
        only black and clear pixels. The alpha channel in the image can be used to adjust the opacity of black
        content, however. This is the recommended way to set an image for the statusbar. Use a 144 DPI resolution to
        support Retina displays. The imageformat can be any of the formats supported by Mac OS X"""
        return self.option('templateImage', value)

    def image(self, value: str) -> Output:
        """Set an image for this item. The image data must be passed as base64 encoded string. Use a 144 DPI
        resolution to support Retina displays. The imageformat can be any of the formats supported by Mac OS X"""
        return self.option('image', value)

    def no_emojize(self):
        """Will disable parsing of github style `:mushroom:` into :mushroom:"""
        return self.option('emojize', self._bool(False))

    def no_ansi(self) -> Output:
        """Turns off parsing of ANSI codes"""
        return self.option('ansi', self._bool(False))

    def option(self, key: str, value: str) -> Output:
        if value:
            self._options.add(OutputOption(key, value))
        return self

    def _bool(self, value: bool) -> str:
        return "true" if value else "false"

    def __str__(self):
        options = ' '.join([str(o) for o in self._options])
        return f'{self.output} | {options}'
