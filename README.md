# xbar Starter for Python

This repo contains a starter for [xbar] plugins written in Python.

![Screenshot](screenshot.png)

## Installation

First, install [xbar].

```shell
open -a xbar
git clone https://gitlab.com/dAnjou/xbar-starter-for-python.git
cd xbar-starter-for-python
REFRESH_TIME=2m make install
# You might have to refresh the plugins in xbar's menu afterwards.
```

If you want to change the prefix or the refresh time, you have to uninstall first:
```shell
make uninstall
```

## How it works

xbar will repeatedly execute scripts in its plugin directory. Ideally these are self-contained which makes them much
easier to maintain and distribute. But for more sophisticated things you need a more sophisticated setup. Here is how it
works with this repo:

![Sequence diagram showing how it works](docs/howitworks.png)

[xbar]: https://xbarapp.com/
